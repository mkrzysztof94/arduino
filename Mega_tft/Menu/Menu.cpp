
#include "Menu.h"


Menu::Menu(){
activ_item=0;
last_action_time = 0;
}

void Menu::Init(){

     Hardware::tft->setTextColor(WHITE);  
  Hardware::tft->setTextSize(1);
  unsigned int i=0;
    for (i = 0; i < 5; i++){
        Hardware::tft->setCursor(10, (((i+1)*10)+(5*i)));
        
        if(activ_item==i){
            Hardware::tft->drawLine(10,(((i+1)*10)+(5*i)-2),120,(((i+1)*10)+(5*i)-2),RED);
            Hardware::tft->setTextColor(MAGENTA);
            Hardware::tft->drawLine(10,(((i+1)*10)+(5*i)+8),120,(((i+1)*10)+(5*i)+8),RED);
            
        }else{
            Hardware::tft->setTextColor(WHITE);  
        }
       Hardware::tft->println( MMenu[i]);
       
    }
}

void Menu::Select(byte mi){
    Hardware::tft->drawLine(10,(((activ_item+1)*10)+(5*activ_item)-2),120,(((activ_item+1)*10)+(5*activ_item)-2),BLACK);
    Hardware::tft->drawLine(10,(((activ_item+1)*10)+(5*activ_item)+8),120,(((activ_item+1)*10)+(5*activ_item)+8),BLACK);
    Hardware::tft->setCursor(10, (((activ_item+1)*10)+(5*activ_item)));
    Hardware::tft->setTextColor(WHITE); 
    Hardware::tft->println( MMenu[activ_item]);
    activ_item=mi;
    Hardware::tft->drawLine(10,(((activ_item+1)*10)+(5*activ_item)-2),120,(((activ_item+1)*10)+(5*activ_item)-2),RED);
    Hardware::tft->setTextColor(MAGENTA);
    Hardware::tft->drawLine(10,(((activ_item+1)*10)+(5*activ_item)+8),120,(((activ_item+1)*10)+(5*activ_item)+8),RED);
    Hardware::tft->setCursor(10, (((activ_item+1)*10)+(5*activ_item)));
    Hardware::tft->println( MMenu[activ_item]);
    Serial1.print("DEBUG : ");
    Serial1.println(activ_item);
}

void Menu::Up(){
    byte a;
    if(activ_item==0){
        a=4;
    }else{
    a=(activ_item-1);
    }
    Select(a);
}

void Menu::Down(){
    byte a;
    if(activ_item==4){
        a=0;
    }else{
    a=(activ_item+1);
    }
    Select(a);
}
void Menu::Update(int* r){
    unsigned long currentMillis = millis();
    Hardware::joy->Lstn();
    if(Hardware::joy->Up() && (currentMillis-last_action_time)>300){
        last_action_time=currentMillis;
        Up();
    }
    if(Hardware::joy->Down() && (currentMillis-last_action_time)>300){
        last_action_time=currentMillis;
        Down();
    }
    if(Hardware::joy->Btn_pressed() && (currentMillis-last_action_time)>300){
       last_action_time=currentMillis;
       EnterSelected();
       *r=2;
    }
}

void Menu::EnterSelected(){
    switch( activ_item ){
        
        case 1:
            AppFileMenager::FileMenager::Run();
            Serial1.println("File Menager Run");
        break;

        default:
            Serial1.print("Run App : ");
            Serial1.println(activ_item);
        break;
    }
}