/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Menu.h
 * Author: Rozrabiaka
 *
 * Created on 6 stycznia 2017, 15:31
 */
#include "../Main.h"
#ifndef MENU_H
#define MENU_H
#include "../Apps/FileMenager/FileMenager.h"
static const String MMenu[]={"Bl","SD","S1","S2","S3"};
class FileMenager;
class Menu{
    public:
        Menu();
        void Init( );
        void Select(byte mi);
        void Up();
        void Down();
        void EnterSelected();
        void Update(int* r);
    private:
        
        byte activ_item;
        unsigned long last_action_time;
};

#endif /* MENU_H */

