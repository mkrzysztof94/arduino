/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * File:   FileMenager.h
 * Author: Rozrabiaka
 *
 * Created on 6 stycznia 2017, 22:06
 */

#include <Arduino.h>
#include <SPI.h>
#include "../../Hardware/SdCtrl/SdCtrl.h"
#include "../../Hardware/SdCtrl/SD/SD.h"
#include "../../Hardware/SdCtrl/SD/utility/SdFat.h"
#include "../../Hardware/SdCtrl/SD/utility/SdFatUtil.h"
//#include <utility/SdFat.h>
//#include <utility/SdFatUtil.h>
#include "../../Main.h"
#include "../OsApp.h"

#ifndef FILEMENAGER_H
#define FILEMENAGER_H

namespace AppFileMenager{
    
class FileMenager : OsApp {
    
public:
    FileMenager();
    FileMenager(const FileMenager& orig);
    virtual ~FileMenager();
    void Draw();
    
    static void Run();
    static void DrawFileList();
    static void Update(int* r);
    static unsigned long last_action_time;
    static byte selected_file;
    static void Down();
    static void Up();
    static void Select(byte mi);
    static void DrawFileContent();
private:
    class File{
        public:
           File(String tmp_name,String tmp_size,String tmp_create_date,String tmp_patch);
           File(String* f);
           String name;
        private:
            
            String size;
            String create_date;
            String patch;
   };
    static File** Files;
    static byte FilesLen;
    static byte FileOpen;
    static byte FileOpenLinesCount;
    static unsigned int FileOpenLinesLongest;
    static String FileOpenContent;
    static String* FileOpenLines;
    static unsigned int FileOpenPos;
    static unsigned int FileOpenOff;
};
   
}
#endif /* FILEMENAGER_H */

