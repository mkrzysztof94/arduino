/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileMenager.cpp
 * Author: Rozrabiaka
 * 
 * Created on 6 stycznia 2017, 22:06
 */
#include "FileMenager.h"


AppFileMenager::FileMenager::FileMenager() {
    
    
}

AppFileMenager::FileMenager::FileMenager(const FileMenager& orig) {
}

AppFileMenager::FileMenager::~FileMenager() {

}
void AppFileMenager::FileMenager::DrawFileContent(){
      // Hardware::tft->fillScreen();
       Hardware::tft->setTextColor(WHITE);
       Hardware::tft->setTextSize(1);
       //17 na linie 
       int i=1;
       int e =12;
       int z;
        byte soff;
        byte zoff;
       for(i;i< e;i++){
            z=i*10;
            unsigned int l=(AppFileMenager::FileMenager::FileOpenPos+i);
            Hardware::tft->fillRect(5, z, (int16_t) 120 , (int16_t) 10, BLACK);
            Hardware::tft->setCursor(5,z);
            Hardware::tft->print(l);
            
            if(i<=10){
                Hardware::tft->print(" ");
            }
            
            Hardware::tft->print(":");
            int len=AppFileMenager::FileMenager::FileOpenLines[l].length();
            len--;
            
            if(AppFileMenager::FileMenager::FileOpenOff < len){
                soff=AppFileMenager::FileMenager::FileOpenOff;
                
                if((AppFileMenager::FileMenager::FileOpenOff+15) <= len){
                    zoff=AppFileMenager::FileMenager::FileOpenOff+15;
                }else{
                    zoff=len;
                }
                Hardware::tft->print(AppFileMenager::FileMenager::FileOpenLines[l].substring(soff,zoff));
            }
            
       }
       
}
void AppFileMenager::FileMenager::Draw(){
    FileMenager::File f = FileMenager::File("a","b","c","d");
    
}
void AppFileMenager::FileMenager::DrawFileList(){
    Hardware::tft->fillScreen();
    Hardware::tft->setCursor(10, 10);
    Serial.println("TO TUTAJ : ");
    
    for (int8_t i = 0; i <= FileMenager::FilesLen; i++){
        
        Hardware::tft->setTextColor(WHITE);
        Hardware::tft->setCursor(10, (((i+1)*10)+(5*i)));
        Hardware::tft->print(FileMenager::Files[i]->name);      
    }
    FileMenager::Select(0);
}
void AppFileMenager::FileMenager::Run(){
    Hardware::tft->fillScreen();
    Hardware::tft->setCursor(10, 10);
     String** s=Hardware::sd->root.GetFiles();
    Serial.println("TO TUTAJ2 : ");
    FileMenager::FilesLen=Hardware::sd->root.GetFilesLen;
    for (int8_t i = 0; i <= FileMenager::FilesLen; i++){
        FileMenager::Files[i]=new FileMenager::File(s[i]);
        Hardware::tft->setTextColor(WHITE);
        Hardware::tft->setCursor(10, (((i+1)*10)+(5*i)));
        Hardware::tft->print(s[i][0]);      
    }
    FileMenager::Select(0);
    //Serial.println(s[5][1]);
}


void AppFileMenager::FileMenager::Select(byte mi){
    byte activ_item = AppFileMenager::FileMenager::selected_file;
    Hardware::tft->drawLine(10,(((activ_item+1)*10)+(5*activ_item)-2),120,(((activ_item+1)*10)+(5*activ_item)-2),BLACK);
    Hardware::tft->drawLine(10,(((activ_item+1)*10)+(5*activ_item)+8),120,(((activ_item+1)*10)+(5*activ_item)+8),BLACK);
    Hardware::tft->setCursor(10, (((activ_item+1)*10)+(5*activ_item)));
    Hardware::tft->setTextColor(WHITE);
    Hardware::tft->print(AppFileMenager::FileMenager::Files[activ_item]->name);

    AppFileMenager::FileMenager::selected_file=(byte)mi;
    activ_item=mi;
    Hardware::tft->drawLine(10,(((activ_item+1)*10)+(5*activ_item)-2),120,(((activ_item+1)*10)+(5*activ_item)-2),RED);
    Hardware::tft->setTextColor(MAGENTA);
    Hardware::tft->drawLine(10,(((activ_item+1)*10)+(5*activ_item)+8),120,(((activ_item+1)*10)+(5*activ_item)+8),RED);
    Hardware::tft->setCursor(10, (((activ_item+1)*10)+(5*activ_item)));
    Hardware::tft->print(AppFileMenager::FileMenager::Files[activ_item]->name);
    
}


void AppFileMenager::FileMenager::Up(){
    if(AppFileMenager::FileMenager::selected_file == 0){
        AppFileMenager::FileMenager::Select((AppFileMenager::FileMenager::FilesLen-1));
    }else{
        AppFileMenager::FileMenager::Select((byte)(AppFileMenager::FileMenager::selected_file-1));
    }
    
}


void AppFileMenager::FileMenager::Down(){
    
    if(AppFileMenager::FileMenager::selected_file == (AppFileMenager::FileMenager::FilesLen-1)){
        AppFileMenager::FileMenager::Select(0);
    }else{
        byte t=(byte)(AppFileMenager::FileMenager::selected_file+1);
        AppFileMenager::FileMenager::Select(t);
    }
    
}

void AppFileMenager::FileMenager::Update(int* r){
    unsigned long currentMillis = millis();
    Hardware::joy->Lstn();
    
    if(Hardware::joy->Up() && (currentMillis-AppFileMenager::FileMenager::last_action_time)>300){
        AppFileMenager::FileMenager::last_action_time=currentMillis;
        if(AppFileMenager::FileMenager::FileOpen==0){
            AppFileMenager::FileMenager::Up();
        }else if(AppFileMenager::FileMenager::FileOpen==1){
            if( AppFileMenager::FileMenager::FileOpenPos > 1){
                AppFileMenager::FileMenager::FileOpenPos--;
                AppFileMenager::FileMenager::DrawFileContent(); 
                
            }
               
            
        }
    }
    
    if(Hardware::joy->Down() && (currentMillis-AppFileMenager::FileMenager::last_action_time)>300){
        AppFileMenager::FileMenager::last_action_time=currentMillis;
        if(AppFileMenager::FileMenager::FileOpen==0){
            AppFileMenager::FileMenager::Down();
        }else if(AppFileMenager::FileMenager::FileOpen==1){
            if( (AppFileMenager::FileMenager::FileOpenPos+11) <= AppFileMenager:: FileMenager::FileOpenLinesCount ){
                AppFileMenager::FileMenager::FileOpenPos++;
                AppFileMenager::FileMenager::DrawFileContent();
            }
        }
    }
    
    if(Hardware::joy->Left() && (currentMillis-AppFileMenager::FileMenager::last_action_time)>100){
         AppFileMenager::FileMenager::last_action_time=currentMillis;
         if(AppFileMenager::FileMenager::FileOpen==1){
             if(AppFileMenager::FileMenager::FileOpenOff>=5){
                 AppFileMenager::FileMenager::FileOpenOff-=5;
                 AppFileMenager::FileMenager::DrawFileContent();
                 
             }
         }
    }
    
    if(Hardware::joy->Right() && (currentMillis-AppFileMenager::FileMenager::last_action_time)>100){
         AppFileMenager::FileMenager::last_action_time=currentMillis;
         if(AppFileMenager::FileMenager::FileOpen==1){
             if(AppFileMenager::FileMenager::FileOpenOff < (AppFileMenager::FileMenager::FileOpenLinesLongest-15)){
                AppFileMenager::FileMenager::FileOpenOff+=5;
                AppFileMenager::FileMenager::DrawFileContent();
             }
        }
    }
    
    
    if(Hardware::joy->Btn_pressed() && (currentMillis-AppFileMenager::FileMenager::last_action_time)>300){
       AppFileMenager::FileMenager::last_action_time=currentMillis;
       
       if(AppFileMenager::FileMenager::FileOpen==0){
            String patch="";
            patch.concat(AppFileMenager::FileMenager::Files[AppFileMenager::FileMenager::selected_file]->name);
            AppFileMenager::FileMenager::FileOpenContent=*Hardware::sd->GetFile(patch);
            int s=0;
            int tmp=0;
            unsigned int i=0;
            unsigned int longes=0;
            while( s < AppFileMenager::FileMenager::FileOpenContent.length()){
                tmp=AppFileMenager::FileMenager::FileOpenContent.indexOf("\n",s);
                AppFileMenager::FileMenager::FileOpenLines[i]=AppFileMenager::FileMenager::FileOpenContent.substring(s,tmp);
                if(AppFileMenager::FileMenager::FileOpenLines[i].length()>longes){
                    longes=AppFileMenager::FileMenager::FileOpenLines[i].length();
                }
                AppFileMenager::FileMenager::FileOpenLinesLongest=longes;
                s=tmp+1;
                if(i>50){
                    break;
                }
                i++;          
                        
            }         
            AppFileMenager::FileMenager::FileOpenLinesCount=i;
            AppFileMenager::FileMenager::FileOpen=1;
            AppFileMenager::FileMenager::FileOpenOff=0;
            AppFileMenager::FileMenager::FileOpenPos=0;
            AppFileMenager::FileMenager::DrawFileContent();
       //EnterSelected();
       }else if(AppFileMenager::FileMenager::FileOpen==1){
           AppFileMenager::FileMenager::FileOpen=0;
            AppFileMenager::FileMenager::DrawFileList();
            
       }
       
    }
    
}

unsigned long AppFileMenager::FileMenager::last_action_time=0;
byte AppFileMenager::FileMenager::selected_file=0;

AppFileMenager::FileMenager::File** AppFileMenager::FileMenager::Files = new AppFileMenager::FileMenager::File*[25];

byte AppFileMenager::FileMenager::FilesLen=0;
byte AppFileMenager::FileMenager::FileOpen=0;
byte AppFileMenager::FileMenager::FileOpenLinesCount=0;
unsigned int AppFileMenager::FileMenager::FileOpenLinesLongest=0;
String* AppFileMenager::FileMenager::FileOpenLines= new String[125];
String AppFileMenager::FileMenager::FileOpenContent="";
unsigned int AppFileMenager::FileMenager::FileOpenPos=0;
unsigned int AppFileMenager::FileMenager::FileOpenOff=0;