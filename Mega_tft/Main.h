/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.h
 * Author: Rozrabiaka
 *
 * Created on 6 stycznia 2017, 23:32
 */
#include <Arduino.h>
#include <SPI.h>
#include <SD.h>

#ifndef MAIN_H
#define MAIN_H


#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0  
#define WHITE   0xFFFF
#include "Hardware/Hardware.h"

// SCLK --> (SCK) --> D13
// MOSI --> (SDA) --> D11
 
#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* MAIN_H */

