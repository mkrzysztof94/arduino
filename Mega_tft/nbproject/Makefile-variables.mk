#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=Ardi-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/Ardi-Windows
CND_ARTIFACT_NAME_Debug=mega_tft
CND_ARTIFACT_PATH_Debug=dist/Debug/Ardi-Windows/mega_tft
CND_PACKAGE_DIR_Debug=dist/Debug/Ardi-Windows/package
CND_PACKAGE_NAME_Debug=megatft.tar
CND_PACKAGE_PATH_Debug=dist/Debug/Ardi-Windows/package/megatft.tar
# Release configuration
CND_PLATFORM_Release=Ardi-Windows
CND_ARTIFACT_DIR_Release=dist/Release/Ardi-Windows
CND_ARTIFACT_NAME_Release=mega_tft
CND_ARTIFACT_PATH_Release=dist/Release/Ardi-Windows/mega_tft
CND_PACKAGE_DIR_Release=dist/Release/Ardi-Windows/package
CND_PACKAGE_NAME_Release=megatft.tar
CND_PACKAGE_PATH_Release=dist/Release/Ardi-Windows/package/megatft.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
