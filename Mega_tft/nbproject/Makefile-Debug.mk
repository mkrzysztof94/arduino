#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=avr-gcc
CCC=avr-g++
CXX=avr-g++
FC=gfortran
AS=avr-as

# Macros
CND_PLATFORM=Ardi-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Apps/FileMenager/FileMenager.o \
	${OBJECTDIR}/Apps/FileMenager/class/File.o \
	${OBJECTDIR}/Apps/OsApp.o \
	${OBJECTDIR}/Hardware/Adafruit_GFX/Adafruit_GFX.o \
	${OBJECTDIR}/Hardware/Adafruit_GFX/glcdfont.o \
	${OBJECTDIR}/Hardware/Adafruit_GFX/glcdfont_ascii.o \
	${OBJECTDIR}/Hardware/Buttons/Buttons.o \
	${OBJECTDIR}/Hardware/Hardware.o \
	${OBJECTDIR}/Hardware/Joystick/Joystick.o \
	${OBJECTDIR}/Hardware/LcdCtrl/LcdCtrl.o \
	${OBJECTDIR}/Hardware/SdCtrl/SD/File.o \
	${OBJECTDIR}/Hardware/SdCtrl/SD/SD.o \
	${OBJECTDIR}/Hardware/SdCtrl/SD/utility/Sd2Card.o \
	${OBJECTDIR}/Hardware/SdCtrl/SD/utility/SdFile.o \
	${OBJECTDIR}/Hardware/SdCtrl/SD/utility/SdVolume.o \
	${OBJECTDIR}/Hardware/SdCtrl/SdCtrl.o \
	${OBJECTDIR}/Hardware/TFT_ILI9163C/TFT_ILI9163C.o \
	${OBJECTDIR}/Menu/Menu.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=${FLAGS_GCC}

# CC Compiler Flags
CCFLAGS=${FLAGS_GPP}
CXXFLAGS=${FLAGS_GPP}

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mega_tft.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mega_tft.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	avr-gcc -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mega_tft ${OBJECTFILES} ${LDLIBSOPTIONS} ${FLAGS_LINKER}

${OBJECTDIR}/Apps/FileMenager/FileMenager.o: Apps/FileMenager/FileMenager.cpp
	${MKDIR} -p ${OBJECTDIR}/Apps/FileMenager
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Apps/FileMenager/FileMenager.o Apps/FileMenager/FileMenager.cpp

${OBJECTDIR}/Apps/FileMenager/class/File.o: Apps/FileMenager/class/File.cpp
	${MKDIR} -p ${OBJECTDIR}/Apps/FileMenager/class
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Apps/FileMenager/class/File.o Apps/FileMenager/class/File.cpp

${OBJECTDIR}/Apps/OsApp.o: Apps/OsApp.cpp
	${MKDIR} -p ${OBJECTDIR}/Apps
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Apps/OsApp.o Apps/OsApp.cpp

${OBJECTDIR}/Hardware/Adafruit_GFX/Adafruit_GFX.o: Hardware/Adafruit_GFX/Adafruit_GFX.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/Adafruit_GFX
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/Adafruit_GFX/Adafruit_GFX.o Hardware/Adafruit_GFX/Adafruit_GFX.cpp

${OBJECTDIR}/Hardware/Adafruit_GFX/glcdfont.o: Hardware/Adafruit_GFX/glcdfont.c
	${MKDIR} -p ${OBJECTDIR}/Hardware/Adafruit_GFX
	${RM} "$@.d"
	$(COMPILE.c) -g -I${INCLUDE} -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/avr/libraries/EEPROM -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/avr/libraries/HID -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/avr/libraries/SoftwareSerial -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/cores/arduino -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/firmwares -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/avr -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/Adafruit_GFX/glcdfont.o Hardware/Adafruit_GFX/glcdfont.c

${OBJECTDIR}/Hardware/Adafruit_GFX/glcdfont_ascii.o: Hardware/Adafruit_GFX/glcdfont_ascii.c
	${MKDIR} -p ${OBJECTDIR}/Hardware/Adafruit_GFX
	${RM} "$@.d"
	$(COMPILE.c) -g -I${INCLUDE} -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/avr/libraries/EEPROM -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/avr/libraries/HID -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/avr/libraries/SoftwareSerial -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/cores/arduino -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/firmwares -I../../arduino-1.6.13-windows/arduino-1.6.13/hardware/arduino/avr -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/Adafruit_GFX/glcdfont_ascii.o Hardware/Adafruit_GFX/glcdfont_ascii.c

${OBJECTDIR}/Hardware/Buttons/Buttons.o: Hardware/Buttons/Buttons.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/Buttons
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/Buttons/Buttons.o Hardware/Buttons/Buttons.cpp

${OBJECTDIR}/Hardware/Hardware.o: Hardware/Hardware.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/Hardware.o Hardware/Hardware.cpp

${OBJECTDIR}/Hardware/Joystick/Joystick.o: Hardware/Joystick/Joystick.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/Joystick
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/Joystick/Joystick.o Hardware/Joystick/Joystick.cpp

${OBJECTDIR}/Hardware/LcdCtrl/LcdCtrl.o: Hardware/LcdCtrl/LcdCtrl.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/LcdCtrl
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/LcdCtrl/LcdCtrl.o Hardware/LcdCtrl/LcdCtrl.cpp

${OBJECTDIR}/Hardware/SdCtrl/SD/File.o: Hardware/SdCtrl/SD/File.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/SdCtrl/SD
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/SdCtrl/SD/File.o Hardware/SdCtrl/SD/File.cpp

${OBJECTDIR}/Hardware/SdCtrl/SD/SD.o: Hardware/SdCtrl/SD/SD.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/SdCtrl/SD
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/SdCtrl/SD/SD.o Hardware/SdCtrl/SD/SD.cpp

${OBJECTDIR}/Hardware/SdCtrl/SD/utility/Sd2Card.o: Hardware/SdCtrl/SD/utility/Sd2Card.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/SdCtrl/SD/utility
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/SdCtrl/SD/utility/Sd2Card.o Hardware/SdCtrl/SD/utility/Sd2Card.cpp

${OBJECTDIR}/Hardware/SdCtrl/SD/utility/SdFile.o: Hardware/SdCtrl/SD/utility/SdFile.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/SdCtrl/SD/utility
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/SdCtrl/SD/utility/SdFile.o Hardware/SdCtrl/SD/utility/SdFile.cpp

${OBJECTDIR}/Hardware/SdCtrl/SD/utility/SdVolume.o: Hardware/SdCtrl/SD/utility/SdVolume.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/SdCtrl/SD/utility
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/SdCtrl/SD/utility/SdVolume.o Hardware/SdCtrl/SD/utility/SdVolume.cpp

${OBJECTDIR}/Hardware/SdCtrl/SdCtrl.o: Hardware/SdCtrl/SdCtrl.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/SdCtrl
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/SdCtrl/SdCtrl.o Hardware/SdCtrl/SdCtrl.cpp

${OBJECTDIR}/Hardware/TFT_ILI9163C/TFT_ILI9163C.o: Hardware/TFT_ILI9163C/TFT_ILI9163C.cpp
	${MKDIR} -p ${OBJECTDIR}/Hardware/TFT_ILI9163C
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Hardware/TFT_ILI9163C/TFT_ILI9163C.o Hardware/TFT_ILI9163C/TFT_ILI9163C.cpp

${OBJECTDIR}/Menu/Menu.o: Menu/Menu.cpp
	${MKDIR} -p ${OBJECTDIR}/Menu
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Menu/Menu.o Menu/Menu.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I${INCLUDE} -ISdCtrl -IAdafruit_GFX -ILcdCtrl -ITFT_ILI9163C -I../../arduino-1.6.13-windows/arduino-1.6.13/libraries -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
