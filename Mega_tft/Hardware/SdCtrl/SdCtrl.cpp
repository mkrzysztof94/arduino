#include "SdCtrl.h"


SdCtrl::SdCtrl(uint8_t cs_pin){
chipSelect=cs_pin;
}
void SdCtrl::Setup(){
   pinMode(chipSelect, OUTPUT);
   
   if(!SD.begin(chipSelect)){
   Serial.println("SD ERROR");
   }
  // we'll use the initialization code from the utility libraries
  // since we're just testing if the card is working!
  if (!card.init(SPI_FULL_SPEED, chipSelect)) {
    Serial.println("Card initialization failed. Things to check:");
    return;
  } 

  // Now we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
  if (!volume.init(card)) {
    Serial.println("Could not find FAT16/FAT32 partition.\nMake sure you've formatted the card");
    return;
  }
   root.openRoot(volume);
  
}
void SdCtrl::ListFiles(){
    root.openRoot(volume);
    root.ls(LS_R | LS_DATE | LS_SIZE);
    String s= root.Getls(LS_R | LS_DATE | LS_SIZE);
    Serial.println("ADI ! :");
    Serial.println(s);
    
}
String SdCtrl::GetListFiles(){
    root.openRoot(volume);
    String s= root.Getls();
    return s;
}
String** SdCtrl::GetFilesList(){
    root.openRoot(volume);
    //String s= ;
    return root.GetFiles();
}


void SdCtrl::OpenFile(String pth){
File dataFile = SD.open(pth.c_str());
    
  // if the file is available, write to it:
  if (dataFile) {
    while (dataFile.available()) {
      Serial.write(dataFile.read());
    }
    dataFile.close();
    Serial.println("");
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening datalog.txt");
  }
}
String* SdCtrl::GetFile(String pth){
    if(!root.isOpen() || !root.isRoot()){
        root.openRoot(volume);
    }
    SdFile s = SdFile();
    String* p = new String();
    
    if(s.open(root,pth.c_str(),O_READ)){
        s.rewind();
        int a=0;        
        while (a!=-1) {
           a= s.read();
            Serial.print((char)a);
            p->concat((char)a);
        }
        s.close();
    }else{
        p->concat("ERROR FILE OPEN");
    }
    return p;
}
String SdCtrl::ob = "";