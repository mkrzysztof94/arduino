/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SdCtrl.h
 * Author: Rozrabiaka
 *
 * Created on 28 grudnia 2016, 00:10
 */


/*  MEGA PINS FOR SD READER! 
 * 52 - CLK
 * 51 MOSI
 * 50 MISO
 * 53 CS
 */
#ifndef SDCTRL_H
#define SDCTRL_H

#include"../Hardware.h"
#include "SD/SD.h"
#include "SD/utility/SdInfo.h"
#include "SD/utility/SdFat.h"
#include "SD/utility/SdFatUtil.h"
#include "SD/utility/Sd2Card.h"
#include "SD/utility/SdInfo.h"

class SdCtrl {
    public:
        SdCtrl(uint8_t cs_pin);
        void Setup();
       void ListFiles();
       String GetListFiles();
       String** GetFilesList();
       void OpenFile(String pth);
       String* GetFile(String pth);
       static String ob;
        Sd2Card card;
        SdVolume volume;
        SdFile root;
    private:
    
    
    uint8_t chipSelect;
    
};


#endif /* SDCTRL_H */

