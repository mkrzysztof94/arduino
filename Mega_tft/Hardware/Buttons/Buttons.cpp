/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Buttons.cpp
 * Author: Rozrabiaka
 * 
 * Created on 6 stycznia 2017, 16:40
 */

#include "Buttons.h"

Buttons::Buttons(uint8_t up,uint8_t down ,uint8_t enter,uint8_t esc) {
    up_pin=up;
    down_pin=down;
    enter_pin=enter;
    esc_pin=esc;
   
}
void Buttons::Setup(){
    pinMode(up_pin, INPUT);
    pinMode(down_pin, INPUT);
    pinMode(enter_pin, INPUT);
    pinMode(esc_pin, INPUT);
}
void Buttons::Lstn(){
    Serial1.print("up: ");
    Serial1.print(digitalRead(up_pin));
    Serial1.print("DOWN: ");
    Serial1.print(digitalRead(down_pin));
    Serial1.print("Enter: ");
    Serial1.print(digitalRead(enter_pin));
    Serial1.print("ESC: ");
    Serial1.print(digitalRead(esc_pin));
    Serial1.println();
    delay(500);
    
    /*
    if(digitalRead(up_pin)==HIGH ){
        Serial1.println("UP CLICKED");
    }
    if(digitalRead(down_pin)==HIGH ){
        Serial1.println("DOWN CLICKED");
    }
    if(digitalRead(enter_pin)==HIGH ){
        Serial1.println("Enter CLICLED");
    }
    if(digitalRead(esc_pin)==HIGH ){
        Serial1.println("Esc CLICKED");
    } */
}

Buttons::Buttons(const Buttons& orig) {
}

Buttons::~Buttons() {
}

