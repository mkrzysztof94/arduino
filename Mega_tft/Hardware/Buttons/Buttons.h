/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Buttons.h
 * Author: Rozrabiaka
 *
 * Created on 6 stycznia 2017, 16:40
 */

#ifndef BUTTONS_H
#define BUTTONS_H
#include <Arduino.h>
class Buttons {
public:
    Buttons(uint8_t up,uint8_t down ,uint8_t enter,uint8_t esc);
    void Setup();
    void Lstn();
    Buttons(const Buttons& orig);
    virtual ~Buttons();
private:
    uint8_t up_pin;
    uint8_t down_pin;
    uint8_t enter_pin;
    uint8_t esc_pin;
};

#endif /* BUTTONS_H */

