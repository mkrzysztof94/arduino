/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Joystick.h
 * Author: Rozrabiaka
 *
 * Created on 6 stycznia 2017, 18:36
 */

#ifndef JOYSTICK_H
#define JOYSTICK_H

class Joystick {
public:
    Joystick(uint8_t pin_x,uint8_t pin_y,uint8_t pin_btn);
    void Lstn();
    bool Up();
    bool Down();
    bool Left();
    bool Right();
    bool Enter();
    bool Esc();
    bool Btn_pressed();
private:
    uint8_t x;
    uint8_t y;
    uint8_t btn;
    int x_val;
    int y_val;
    int button_state;
};

#endif /* JOYSTICK_H */

