/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Joystick.cpp
 * Author: Rozrabiaka
 * 
 * Created on 6 stycznia 2017, 18:36
 */

#include <stdint.h>
#include <Arduino.h>

#include "Joystick.h"

Joystick::Joystick(uint8_t pin_x,uint8_t pin_y,uint8_t pin_btn) {
    x=pin_x;
    y=pin_y;
    btn=pin_btn;
}
void Joystick::Lstn(){  
    x_val=analogRead(x);
    y_val=analogRead(y);
    button_state=analogRead(btn);
}

bool Joystick::Btn_pressed(){
    if(button_state < 100 ){
        return true;
    }else{
        return false;
    }
}

bool Joystick::Up(){
    if(y_val <450){
    return true;
    }else{
    return false;
    }
}
bool Joystick::Down(){
    if(y_val >650){
    return true;
    }else{
    return false;
    }
}
bool Joystick::Left(){
    if(x_val <450){
    return true;
    }else{
    return false;
    }
}
bool Joystick::Right(){
    if(x_val >650){
    return true;
    }else{
    return false;
    }
}
bool Joystick::Enter(){
    if(x_val >650){
    return true;
    }else{
    return false;
    }
}
bool Joystick::Esc(){
    if(x_val <450){
    return true;
    }else{
    return false;
    }
}
