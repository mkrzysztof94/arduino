/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Hardware.h
 * Author: Rozrabiaka
 *
 * Created on 7 stycznia 2017, 00:00
 */

#ifndef HARDWARE_H
#define HARDWARE_H




#include "TFT_ILI9163C/TFT_ILI9163C.h"
#include "LcdCtrl/LcdCtrl.h"
#include "Buttons/Buttons.h"
#include "Joystick/Joystick.h"
#include "SdCtrl/SdCtrl.h"

#define __DC 33
#define __CS 53
#define __RST 35


 class Joystick;
 class SdCtrl;
 class Hardware{
    public:
    Hardware();
    
    static TFT_ILI9163C* tft;
    static Joystick* joy;
    static SdCtrl* sd;
    static Hardware* Devices;
    static LcdCtrl* lcd;
    private:

     
};

#endif /* HARDWARE_H */

