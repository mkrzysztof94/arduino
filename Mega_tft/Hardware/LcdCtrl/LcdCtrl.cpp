#include "LcdCtrl.h"


LcdCtrl::LcdCtrl(uint8_t LCD_PIN_SCE_C,uint8_t LCD_PIN_RESET_C,uint8_t LCD_PIN_DC_C,uint8_t LCD_PIN_SDIN_C,uint8_t LCD_PIN_SCLK_C) {
LCD_PIN_SCE=LCD_PIN_SCE_C;
LCD_PIN_RESET=LCD_PIN_RESET_C;
LCD_PIN_DC=LCD_PIN_DC_C;
LCD_PIN_SDIN=LCD_PIN_SDIN_C;
LCD_PIN_SCLK=LCD_PIN_SCLK_C;
 
}
void LcdCtrl::Setup(void){
  pinMode(LCD_PIN_SCE, OUTPUT);
  pinMode(LCD_PIN_RESET, OUTPUT);
  pinMode(LCD_PIN_DC, OUTPUT);
  pinMode(LCD_PIN_SDIN, OUTPUT);
  pinMode(LCD_PIN_SCLK, OUTPUT);
  digitalWrite(LCD_PIN_RESET, LOW);
  digitalWrite(LCD_PIN_RESET, HIGH);
  LcdWrite(LCD_C, 0x21 );  // LCD Extended Commands.
  LcdWrite(LCD_C, 0xBF );  // Set LCD Vop (Contrast). 
  LcdWrite(LCD_C, 0x04 );  // Set Temp coefficent. //0x04
  LcdWrite(LCD_C, 0x14 );  // LCD bias mode 1:48. //0x13
  LcdWrite(LCD_C, 0x20 );  // LCD Basic Commands
  LcdWrite(LCD_C, 0x0C );  // LCD in normal mode.
  
  
}
void LcdCtrl::LcdWrite(byte dc, byte data)
{
  digitalWrite(LCD_PIN_DC, dc);
  digitalWrite(LCD_PIN_SCE, LOW);
  shiftOut(LCD_PIN_SDIN, LCD_PIN_SCLK, MSBFIRST, data);
  digitalWrite(LCD_PIN_SCE, HIGH);
}

void LcdCtrl::LcdCharacter(char character)
{
  LcdWrite(LCD_D, 0x00);
  for (int index = 0; index < 5; index++)
  {
    LcdWrite(LCD_D, ASCII[character - 0x20][index]);
  }
  LcdWrite(LCD_D, 0x00);
}

void LcdCtrl::LcdClear(void)
{
  for (int index = 0; index < LCD_X * LCD_Y / 8; index++)
  {
    LcdWrite(LCD_D, 0x00);
  }
}

void LcdCtrl::LcdString(char *characters)
{
  while (*characters)
  {
    LcdCharacter(*characters++);
  }
}
void LcdCtrl::MenuInit(){
     unsigned int i=0;
     unsigned int b=0;
     
     LcdClear();
    for (i = 0; i < 5; i++){
        char a[25];
//        Menu[i].toCharArray(a,25);
        LcdString(a);
    }
}
void LcdCtrl::MenuUp(){

}
void LcdCtrl::MenuDown(){
    
}
