#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=avr-gcc
CCC=avr-g++
CXX=avr-g++
FC=gfortran
AS=avr-as

# Macros
CND_PLATFORM=Arduiuno-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/BluetoothCtrl/BtCtrl.o \
	${OBJECTDIR}/LcdCtrl/LcdCtrl.o \
	${OBJECTDIR}/LedCtrl/LedCtrl.o \
	${OBJECTDIR}/PcCtrl/PcCtrl.o \
	${OBJECTDIR}/SdCtrl/SdCtrl.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/main_ctrl.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/main_ctrl.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/main_ctrl ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/BluetoothCtrl/BtCtrl.o: BluetoothCtrl/BtCtrl.cpp 
	${MKDIR} -p ${OBJECTDIR}/BluetoothCtrl
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BluetoothCtrl/BtCtrl.o BluetoothCtrl/BtCtrl.cpp

${OBJECTDIR}/LcdCtrl/LcdCtrl.o: LcdCtrl/LcdCtrl.cpp 
	${MKDIR} -p ${OBJECTDIR}/LcdCtrl
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/LcdCtrl/LcdCtrl.o LcdCtrl/LcdCtrl.cpp

${OBJECTDIR}/LedCtrl/LedCtrl.o: LedCtrl/LedCtrl.cpp 
	${MKDIR} -p ${OBJECTDIR}/LedCtrl
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/LedCtrl/LedCtrl.o LedCtrl/LedCtrl.cpp

${OBJECTDIR}/PcCtrl/PcCtrl.o: PcCtrl/PcCtrl.cpp 
	${MKDIR} -p ${OBJECTDIR}/PcCtrl
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PcCtrl/PcCtrl.o PcCtrl/PcCtrl.cpp

${OBJECTDIR}/SdCtrl/SdCtrl.o: SdCtrl/SdCtrl.cpp 
	${MKDIR} -p ${OBJECTDIR}/SdCtrl
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SdCtrl/SdCtrl.o SdCtrl/SdCtrl.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/main_ctrl.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
