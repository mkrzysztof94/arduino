/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SdCtrl.h
 * Author: Rozrabiaka
 *
 * Created on 28 grudnia 2016, 00:10
 */

#ifndef SDCTRL_H
#define SDCTRL_H

#include <Arduino.h>
#include <SPI.h>
#include<SD.h>
class SdCtrl {
    public:
        SdCtrl(uint8_t cs_pin);
        void 
                Setup();
    private:
    Sd2Card card;
    SdVolume volume;
    SdFile root;
    uint8_t chipSelect;
    
};


#endif /* SDCTRL_H */

