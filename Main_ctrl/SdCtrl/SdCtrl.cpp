#include "SdCtrl.h"


SdCtrl::SdCtrl(uint8_t cs_pin){
chipSelect=cs_pin;
}
void SdCtrl::Setup(){
   pinMode(chipSelect, OUTPUT);
   SD.begin(chipSelect);
   Serial.print("\nInitializing SD card...");

  // we'll use the initialization code from the utility libraries
  // since we're just testing if the card is working!
  if (!card.init(SPI_HALF_SPEED, chipSelect)) {
    Serial.println("Card initialization failed. Things to check:");
    return;
  } else {
    Serial.println("OK");
  }

  // print the type of card
  Serial.print("\nCard type: ");
  switch (card.type()) {
    case SD_CARD_TYPE_SD1:
      Serial.println("SD1");
      break;
    case SD_CARD_TYPE_SD2:
      Serial.println("SD2");
      break;
    case SD_CARD_TYPE_SDHC:
      Serial.println("SDHC");
      break;
    default:
      Serial.println("Unknown");
  }

  // Now we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
  if (!volume.init(card)) {
    Serial.println("Could not find FAT16/FAT32 partition.\nMake sure you've formatted the card");
    return;
  }
  root.openRoot(volume);
  
  //SD.mkdir("a/b/c") ;
File dataFile = SD.open("/TEST.TXT");

  // if the file is available, write to it:
  if (dataFile) {
    while (dataFile.available()) {
      Serial.write(dataFile.read());
    }
    dataFile.close();
    Serial.println("");
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening datalog.txt");
  }
  // list all files in the card with date and size
  root.ls(LS_R | LS_DATE | LS_SIZE);
}