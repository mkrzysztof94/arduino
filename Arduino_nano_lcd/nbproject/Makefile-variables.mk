#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=Arduiuno-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/Arduiuno-Windows
CND_ARTIFACT_NAME_Debug=arduino_nano_lcd
CND_ARTIFACT_PATH_Debug=dist/Debug/Arduiuno-Windows/arduino_nano_lcd
CND_PACKAGE_DIR_Debug=dist/Debug/Arduiuno-Windows/package
CND_PACKAGE_NAME_Debug=arduinonanolcd.tar
CND_PACKAGE_PATH_Debug=dist/Debug/Arduiuno-Windows/package/arduinonanolcd.tar
# Release configuration
CND_PLATFORM_Release=Arduiuno-Windows
CND_ARTIFACT_DIR_Release=dist/Release/Arduiuno-Windows
CND_ARTIFACT_NAME_Release=arduino_nano_lcd
CND_ARTIFACT_PATH_Release=dist/Release/Arduiuno-Windows/arduino_nano_lcd
CND_PACKAGE_DIR_Release=dist/Release/Arduiuno-Windows/package
CND_PACKAGE_NAME_Release=arduinonanolcd.tar
CND_PACKAGE_PATH_Release=dist/Release/Arduiuno-Windows/package/arduinonanolcd.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
