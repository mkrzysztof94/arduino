#include <Arduino.h>
#include <SoftwareSerial.h>

extern HardwareSerial Serial;
SoftwareSerial BTSerial(2,3); // RX | TX

void setup()
{

  Serial.begin(9600);
  Serial.println("Enter AT commands:");
  BTSerial.begin(38400);  // HC-05 default speed in AT command more
}

void loop()
{
  if (BTSerial.available())
    Serial.print((char)BTSerial.read());

  if (Serial.available())
    BTSerial.write(Serial.read());
}